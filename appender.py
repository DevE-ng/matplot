import json
import sys


def write_json(entry, filename='values.json'):
    storageDepth = 5
    print("started")
    if not is_json(entry):
        print("Incorrect json")
        return

    new_data = json.loads(entry)
    with open(filename,'r+') as file:
        file_data = json.load(file)
        file.close()

        file_data["x"].append(new_data["x"])
        x = file_data["x"]
        file_data["y"].append(new_data["y"])
        y = file_data["y"]
        xlen = len(x)
        print(xlen)
        if xlen > storageDepth:
            del x[0:xlen-storageDepth]
            del y[0:xlen-storageDepth]

        file_data["x"] = x
        file_data["y"] = y

        json_file = open("values.json", "w+")
        json_file.write(json.dumps(file_data))
        json_file.close()


def is_json(myjson):
    try:
        json.loads(myjson)
    except ValueError as e:
        return False
    return True

val = sys.argv[1]
write_json(val)