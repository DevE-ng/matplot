import matplotlib.pyplot as plt
import json
import time
from time import mktime
from datetime import datetime

def toTime(timestamp):
    dt_object = datetime.fromtimestamp(timestamp)
    return dt_object.strftime('%H:%M:%S')

while True:
    try:
        with open('values.json') as f:
            d = json.load(f)

            print(d)
            x = list(map(toTime,d["x"]))
            y = d["y"]

            plt.plot(x, y, color='green', marker='o', markersize=7)
            plt.xlabel('Время')
            plt.ylabel('Значение')

            #plt.show(block=False)
            plt.draw()
            plt.pause(0.001)
            #plt.show()
    except OSError:
        print("Could not open/read file")
    time.sleep(1)